// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


package kotlin

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import kotlin.helpers.TestClass
import kotlin.helpers.TestDataClass

class StdlibExtensionsTest {

    @Test
    fun ifTrueTest() {
        val result = true.ifTrue {
            true
        }
        assertThat(result)
            .isTrue
    }

    @Test
    fun ifTrueFailTest() {
        val result = false.ifTrue {
            true
        }
        assertThat(result)
            .isNull()
    }

    @Test
    fun ifFalseTest() {
        val result = false.ifFalse {
            true
        }
        assertThat(result)
            .isTrue
    }

    @Test
    fun ifFalseFailTest() {
        val result = true.ifFalse {
            true
        }
        assertThat(result)
            .isNull()
    }

    @Test
    fun isDataClass() {
        assertThat(TestDataClass("init").isData())
            .isTrue
    }

    @Test
    fun isNotDataClass() {
        assertThat(TestClass().isData())
            .isFalse
    }

    @Test
    fun alsoIfTrueTest() {
        val result = TestClass().alsoIf(true) {
            it.checkField = "changed"
        }
        assertThat(result.checkField)
            .isEqualTo("changed")
    }

    @Test
    fun alsoIfFalseTest() {
        val result = TestClass().alsoIf(false) {
            it.checkField = "changed"
        }
        assertThat(result.checkField)
            .isEqualTo("init")
    }

    @Test
    fun applyIfTrueTest() {
        val result = TestClass().applyIf(true) {
            checkField = "changed"
        }
        assertThat(result.checkField)
            .isEqualTo("changed")
    }

    @Test
    fun applyIfFalseTest() {
        val result = TestClass().applyIf(false) {
            checkField = "changed"
        }
        assertThat(result.checkField)
            .isEqualTo("init")
    }

    @Test
    fun letIfTrueTest() {
        val result = "1".letIf(true) {
            1
        }
        assertThat(result)
            .isEqualTo(1)
    }

    @Test
    fun letIfFalseTest() {
        val result = "1".letIf(false) {
            1
        }
        assertThat(result)
            .isEqualTo("1")
    }

    @Test
    fun runIfTrueTest() {
        val result = TestDataClass("string").runIf(true) {
            this.s
        }
        assertThat(result)
            .isInstanceOf(String::class.java)
            .isEqualTo("string")
    }

    @Test
    fun runIfFalseTest() {
        val result = TestDataClass("string").runIf(false) {
            TestClass()
        }
        assertThat(result)
            .isInstanceOf(TestDataClass::class.java)
    }
}
