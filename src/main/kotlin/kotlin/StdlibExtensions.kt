// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


package kotlin

/**
 * Evaluate lambda if boolean is true.
 *
 * @return result of lambda.
 */
inline fun <T> Boolean.ifTrue(block: () -> T?): T? = if (this) block() else null

/**
 * Evaluate lambda if boolean is false.
 *
 * @return result of lambda.
 */
inline fun <T> Boolean.ifFalse(block: () -> T?): T? = if (!this) block() else null

/**
 * Checking if an instance is a data class.
 *
 * @return result of checking.
 */
inline fun <reified T : Any> T.isData(): Boolean = this::class.isData

/**
 * Manipulate with objects by condition.
 *
 * @return self.
 */
inline fun <T> T.applyIf(condition: Boolean, block: T.() -> Unit): T = if (condition) {
    block()
    this
} else this

/**
 * Run expression by condition
 */
inline fun <T> T.alsoIf(condition: Boolean, block: (T) -> Unit): T = if (condition) {
    block(this)
    this
} else this

/**
 * Run a code [block] with this as an argument and return result if [condition] is true, or else return self
 *
 * @return result or self
 */
inline fun <T, R> T.letIf(condition: Boolean, block: (T) -> R): R where T : R = if (condition) {
    block(this)
} else this

/**
 * Run a code [block] if [condition] is true and return result, or else return self
 *
 * @return result or self
 */
inline fun <T, R> T.runIf(condition: Boolean, block: T.() -> R): R where T : R = if (condition) {
    block()
} else this
